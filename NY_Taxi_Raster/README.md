# Demo code for hadoop tutorial

## Content
Do query and rasterization for radiation dataset

## Progress
- [x] output "$rowIndex,$colIndex:$meanGridValue" ( result approved by spark)
- [ ] transform output to ascii grid

## Sample Run (prepare input and output files first)

hadoop jar dist/queryNYTaxi.jar classes/runhadoop/RunRadQuery.class /user/root/trip_data_1.csv processed.csv taxiImage.asc 40.479636 40.930724 -74.402322 -73.630027 0.005


## New York Boundingbox Info:
## startLat, endLat, startLng, stopLng
40.479636,40.930724,-74.402322,-73.630027,0.005
##(Lower left corner: 40.479636, -74.402322; Upper right corner: 40.930724, -73.630027)
where the arguments represent for inputFile, intermediateFile, outputFile, startLat, endLat, startLong, endLong, cellSize
